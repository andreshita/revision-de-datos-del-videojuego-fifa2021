# Revisión de datos del videojuego FIFA2021

## Contexto
Como estudiantes del curso de Visualización y Storytelling del MIAD nos propusimos hacer una revisión detallada de los jugadores de futbol del videojuego FIFA2021 y así, exponer estos análisis para quienes disfrutamos del videojuego. El objetivo, es exponer una panorámica de los jugadores y  sus atributos, adicional, revisar como están distribuídos estos, de acuerdo a los clubes donde participan y las regiones donde están. Se plantean otras revisiones que más adelantes revisaremos especifícamente.

La fuente de datos original, [FIFA 2021 Complete Player Dataset](https://www.kaggle.com/aayushmishra1512/fifa-2021-complete-player-data), se obtuvo de Kaggle: 
"El conjunto de datos contiene información sobre la calificación de los jugadores, su edad, su nacionalidad, la posición en la que juegan y su potencial de crecimiento en el juego. Es posible que los datos de algunos jugadores y sus clubes no sean muy precisos, ya que la ventana de transferencia aún está abierta y es posible que se realicen cambios en etapas posteriores." Kaggle.
La información se cargó en este repositorio. [Ver archivo](https://gitlab.com/andreshita/revision-de-datos-del-videojuego-fifa2021/-/blob/main/FIFA-21_Complete__1_.csv).

## Revision de los datos de jugadores del videojuego FIFA2021
Este proyecto inicia con el pre-procesamiento de la base de datos en Python. Los comandos exactos requeridos se encuentran dentro del archivo [Modelado_datos_Preprocessing.ipynb](https://gitlab.com/andreshita/revision-de-datos-del-videojuego-fifa2021/-/blob/main/Modelado_datos_Preprocessing.ipynb), y aquí describiremos los pasos que se fueron siguiendo de manera genérica para dejar la base de datos preparada para su uso en Google Datastudio.
- **Exploración de los datos:** Identificación de las variables, tipos y rangos, cantidad de países y cantidad de clubes. Hay que notar que la base de datos no tenía datos faltantes en su forma original. Las variables en su forma original son: *player_id, name, nationality, position, overall, age, hits, potential, team*; que representan respectivamente: código de jugador, nombre, nacionalidad, posición en el juego, puntaje global, edad, goles, potencial y equipo. Las únicas variables de tipo caracter son *name, nationality, position* y *team*; las otras variables son de tipo numérico.  
- **Homogenización y limpieza de los datos:** Se decide trabajar las variables en español y para ello se traducen los países, los nombres de las diferentes columnas. Se agrega una columna con un cálculo aproximado del salario de cada jugador (en dólares) al mes *salario_aprox*. La formula utilizada para realizar el cálculo del salario fue inspirada en la información contenida en [THEMONEY](https://themoney.co/es/comment-calculer-le-salaire-dun-footballeur/). Se crea la columna de cada posición (*Posición*): si el jugador la puede ocupar se le asigna un **1** y si no la puede ocupar se le asigna un **0**. También se cambia el nombre de las columnas de la posición de las siglas en inglés a su nombre completo en español (los nombres de las difererntes posiciones en español se pueden hallar en [deporfit](https://deporfit.com/fifa-posicion-ultimate-team-ingles-espanol/).
- **Exportación de la base de datos aumentada o procesada:** Finalmente se exporta la base de datos procesada par su utilización en Google Datastudio en formato *csv*.  

Adicionalmente, después de extraer la base de datos procesada de Python se necesito realizar algunos cambios directamente sobre el archivo en Google Sheets:  
- **Añadir una columna para rango de edad** (*rango_edad*): se hizo necesario realizar a partir de filtros está asignación debido a que había una amplia variedad de edades que al ser utilizadas en gráficas generaba un patrón de valores difícil de visualizar.
- **Añadir una columna para puntaje global a partir de estrellas** (*Estrellas*): se incorporó una función lineal que llevara los puntajes globales de cada jugador de una escala de 56-94 a una de 1-5, que simbolizaría la cantidad de estrellas que se mostraría.
- **Creación de thumbnail** (*Foto_Equipo*): Se crea una nueva variable donde se tiene la URL de la imágen de cada uno de los escudos de los clubes, con la finalidad de incluirlos en las tablas dinámica al interior del dashboard de Google DataStudio para facilitar la identificación visual de los fanaticos FIFA, estas imágenes se extraen de [Fifa Index](https://www.fifaindex.com/).
- **Creación de Posiciones** (*Posicion*): A partir del archivo *csv* donde se realizarón las transformaciones anteriormente escritas, se realiza la creación de un nuevo archivo que contenga la totalidad de la información de la posición de cada de los jugadores en la misma columna, y se homologan estas siglas con su respectivo nombre en español.  

Finalmente al interior del Google DataStudio, se crea una nueva variable que se llama (*Puntaje FIFA21*) donde se crea un Start Ranking a partir de la variable estrella realizada al interior del Google Sheet, se programa de la siguiente manera:
`CASE
WHEN Estrellas = 1 THEN "⭐"
WHEN Estrellas = 2 THEN "⭐⭐"
WHEN Estrellas = 3 THEN "⭐⭐⭐"
WHEN Estrellas = 4 THEN "⭐⭐⭐⭐"
WHEN Estrellas = 5 THEN "⭐⭐⭐⭐⭐"
END`

## Instalación
Para la ejecución del script que hace el pre-procesamiento antes mencionado, es requerido la instalación de [Jupyter Notebook](https://docs.jupyter.org/en/latest/install.html).
Las librerías de Python requeridas para hacer el setup son:
- pandas
- google.colab

## Visualización
La visualización de los datos se puede revisar directamente en Google DataStudio en el dashboard: [Datastudio](https://datastudio.google.com/s/hu4lu13u7_g)


## Presupuesto
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
![Presupuesto](https://gitlab.com/-/ide/project/andreshita/revision-de-datos-del-videojuego-fifa2021/tree/alejitamatu-main-patch-27537/-/Presupesto.png/)

## Autores and reconocimientos

Una publicación de Contenido con Sentido Group --- MIAD UniAndes. --- Curso Visualización y Storytelling -- 2022
Integrantes:
- Misael García Díaz
- Angie Hidalgo
- Alejandra Maturana
- Andrés Hita

## Licencia
Open source 

## Estado del proyecto
En desarrollo

